// Introduction to NodeJS

let http = require("http");
// modules needs to be imported or required 

console.log(http);
	//http module containts methods and other codes which allowed us to create server and let our client communicate through HTTP

//createServer()- it creates server that handles requests and responses.
	// .createServer()- method has an anonymous function that handles our client and our server response. The anonymosue function in the createServer method is able to receive two objects, first, req or request from the client. Second, res or reponse, this is our server response.
	// Each request and response parameters are object which contains the details of a request and response as well the method to handle them

	//res.writeHead- a method of the response object. This will allow us to add headers, which are additional information about our server's response. The first argument in writeHead is a HTTP status which is used to tell the client about the status of their requests.

	//res.end()- is a method of a response object which ends the server's response and sends a message as a string.

	//.listen()- assigns a port to a server. There are several tasks and process in our computer which are also designated into their specific ports.

	// http://localhost:4000
		//localhost- current machine 
		//4000- port number assigned to where the process/server is listening/running from.

http.createServer((req, res) => {
	//localhost:4000/- endpoint
	if(req.url ==="/"){
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Hi! Welcome to our Homepage")
	//localhost:4000/login
	} else if (req.url === "/login"){
		res.writeHead(200, {"Content-Type" : "text/plain"})
		res.end("Welcome to the login page!")
	} else {
		res.writeHead(404, {"Content-Type" : "text/plain"})
		res.end("Resource cannot be found")
	}
}).listen(4000);
console.log("Server is running on localhost: 4000");